package org.epis.owasp;

import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class OWASPAlgorithm implements Serializable
{
    private static final long serialVersionUID = -5138099603990743820L;
    
    public static SecuredMessage packMessage(String message, PrivateKey prvKeySender, PublicKey pubKeyReceiver) throws OWASPException
    {
        return packMessage(message, prvKeySender, pubKeyReceiver, "MD5", "MD5withRSA");
    }
    
    public static SecuredMessage packMessage(String message, PrivateKey prvKeySender, PublicKey pubKeyReceiver, String hashingAlgorithm, String signatureAlgorithm) throws OWASPException
    {
        try
        {
            SecuredMessage securedMessage = new SecuredMessage();
            HeaderSecuredMessage headerSecuredMessage = new HeaderSecuredMessage();
            
            SecretKey secretKey = OWASPUtil.generateRandomSecretKey();
            byte[] encryptedMessage = OWASPUtil.encryptAES(message.getBytes(), secretKey);
            securedMessage.setEncryptedMessage(encryptedMessage);
            byte[] encryptedSymmetricKey = OWASPUtil.encryptRSA(secretKey.getEncoded(), pubKeyReceiver);
            headerSecuredMessage.setEncryptedSymmetricKey(encryptedSymmetricKey);
            
            MessageDigest md = MessageDigest.getInstance(hashingAlgorithm);
            md.update(message.getBytes());
            headerSecuredMessage.setHashMessage(OWASPUtil.bytesToString(md.digest()));
            
            securedMessage.setHeader(headerSecuredMessage);
            
            byte[] signature = signHeader(headerSecuredMessage, prvKeySender, signatureAlgorithm);
            securedMessage.setDigitalSignature(signature);
            
            return securedMessage;
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new OWASPException("Hashing Algorithm " + hashingAlgorithm + " not supported.", "HASHING_NOT_SUPPORTED");
        }
    }
    
    public static String unpackMessage(SecuredMessage securedMessage, PrivateKey prvKeyReceiver) throws OWASPException
    {
        byte[] symmetricKey = OWASPUtil.decryptRSA(securedMessage.getHeader().getEncryptedSymmetricKey(), prvKeyReceiver);
        SecretKeySpec secretKey = new SecretKeySpec(symmetricKey, "AES");
        return new String(OWASPUtil.decryptAES(securedMessage.getEncryptedMessage(), secretKey));
    }

    private static byte[] signHeader(HeaderSecuredMessage header, PrivateKey prvKeySender, String signatureAlgorithm) throws OWASPException
    {
        try
        {
            Signature mySign = Signature.getInstance(signatureAlgorithm);
            mySign.initSign(prvKeySender);
            mySign.update(header.toString().getBytes());
            return mySign.sign();
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new OWASPException("Digital Signature Algorithm " + signatureAlgorithm + " not supported", "DIGITAL_SIGNATURE_NOT_SUPPORTED");
        }
        catch (InvalidKeyException ex)
        {
            throw new OWASPException("Invalid receiver private key for digital signature", "INVALID_PRIVATE_KEY");
        }
        catch (SignatureException ex)
        {
            throw new OWASPException("Signature exception", "SIGNATURE_ERROR");
        }
    }
    
    public static boolean validateSignature(SecuredMessage securedMessage, PublicKey pubKeySender) throws OWASPException
    {
        return validateSignature(securedMessage, pubKeySender, "MD5withRSA");
    }
    
    public static boolean validateSignature(SecuredMessage securedMessage, PublicKey pubKeySender, String signatureAlgorithm) throws OWASPException
    {
        try
        {
            Signature myVerifySign = Signature.getInstance(signatureAlgorithm);
            myVerifySign.initVerify(pubKeySender);
            myVerifySign.update(securedMessage.getHeader().toString().getBytes());
            return myVerifySign.verify(securedMessage.getDigitalSignature());
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new OWASPException("Digital Signature Algorithm " + signatureAlgorithm + " not supported", "DIGITAL_SIGNATURE_NOT_SUPPORTED");
        }
        catch (InvalidKeyException ex)
        {
            throw new OWASPException("Invalid sender public key for digital signature", "INVALID_PUBLIC_KEY");
        }
        catch (SignatureException ex)
        {
            throw new OWASPException("Signature exception", "SIGNATURE_ERROR");
        }
    }
    
    public static boolean validateHashing(SecuredMessage securedMessage, String decryptedMessage) throws OWASPException
    {
        return validateHashing(securedMessage, decryptedMessage, "MD5");
    }
    
    public static boolean validateHashing(SecuredMessage securedMessage, String decryptedMessage, String hashingAlgorithm) throws OWASPException
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance(hashingAlgorithm);
            md.update(decryptedMessage.getBytes());
            return OWASPUtil.bytesToString(md.digest()).equals(securedMessage.getHeader().getHashMessage());
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new OWASPException("Hashing Algorithm " + hashingAlgorithm + " not supported.", "HASHING_NOT_SUPPORTED");
        }
    }
}
