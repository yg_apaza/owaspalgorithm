package org.epis.owasp;

import java.io.Serializable;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.security.Key;

import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

public class OWASPUtil implements Serializable
{
    private static final long serialVersionUID = 8353725645121770142L;
    
    private static KeyGenerator keyGen;

    protected static SecretKey generateRandomSecretKey() throws OWASPException
    {
        try
        {
            keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
            SecretKey secretKey = keyGen.generateKey();
            return secretKey;
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new OWASPException("AES algorithm not supported", "AES_NOT_SUPPORTED");
        }
    }
    
    protected static byte[] encryptAES(byte[] byteDataToEncrypt, Key secretKey) throws OWASPException
    {
        try
        {
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey, aesCipher.getParameters());
            return aesCipher.doFinal(byteDataToEncrypt);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            throw new OWASPException("AES algorithm not supported", "AES_NOT_SUPPORTED");
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            throw new OWASPException("No Such Padding exists", "NO_SUCH_PADDING");
        }
        catch (InvalidKeyException invalidKey)
        {
            throw new OWASPException("Invalid Key", "INVALID_KEY");
        }
        catch (BadPaddingException badPadding)
        {
            throw new OWASPException("Bad Padding", "BAD_PADDING");
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            throw new OWASPException("Illegal Block Size", "ILLEGAL_BLOCK_SIZE");
        }
        catch (InvalidAlgorithmParameterException invalidParam)
        {
            throw new OWASPException("Invalid Algorithm Parameter", "INVALID_PARAMETER");
        }
    }
    
    protected static byte[] decryptAES(byte[] byteCipherText, Key secretKey) throws OWASPException
    {
        try
        {
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.DECRYPT_MODE, secretKey, aesCipher.getParameters());
            return aesCipher.doFinal(byteCipherText);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            throw new OWASPException("AES algorithm not supported", "AES_NOT_SUPPORTED");
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            throw new OWASPException("No Such Padding exists", "NO_SUCH_PADDING");
        }
        catch (InvalidKeyException invalidKey)
        {
            throw new OWASPException("Invalid Key", "INVALID_KEY");
        }
        catch (BadPaddingException badPadding)
        {
            throw new OWASPException("Bad Padding", "BAD_PADDING");
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            throw new OWASPException("Illegal Block Size", "ILLEGAL_BLOCK_SIZE");
        }
        catch (InvalidAlgorithmParameterException invalidParam)
        {
            throw new OWASPException("Invalid Algorithm Parameter", "INVALID_PARAMETER");
        }
    }
    
    protected static byte[] encryptRSA(byte[] byteDataToEncrypt, Key secretKey) throws OWASPException
    {
        try
        {
            Cipher aesCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return aesCipher.doFinal(byteDataToEncrypt);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            throw new OWASPException("RSA/ECB/PKCS1Padding algorithm not supported", "RSA_NOT_SUPPORTED");
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            throw new OWASPException("No Such Padding exists", "NO_SUCH_PADDING");
        }
        catch (InvalidKeyException invalidKey)
        {
            throw new OWASPException("Invalid Key", "INVALID_KEY");
        }
        catch (BadPaddingException badPadding)
        {
            throw new OWASPException("Bad Padding", "BAD_PADDING");
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            throw new OWASPException("Illegal Block Size", "ILLEGAL_BLOCK_SIZE");
        }
    }
    
    protected static byte[] decryptRSA(byte[] byteCipherText, Key secretKey) throws OWASPException
    {
        try
        {
            Cipher aesCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
            return aesCipher.doFinal(byteCipherText);
        }
        catch (NoSuchAlgorithmException noSuchAlgo)
        {
            throw new OWASPException("RSA/ECB/PKCS1Padding algorithm not supported", "RSA_NOT_SUPPORTED");
        }
        catch (NoSuchPaddingException noSuchPad)
        {
            throw new OWASPException("No Such Padding exists", "NO_SUCH_PADDING");
        }
        catch (InvalidKeyException invalidKey)
        {
            throw new OWASPException("Invalid Key", "INVALID_KEY");
        }
        catch (BadPaddingException badPadding)
        {
            throw new OWASPException("Bad Padding", "BAD_PADDING");
        }
        catch (IllegalBlockSizeException illegalBlockSize)
        {
            throw new OWASPException("Illegal Block Size", "ILLEGAL_BLOCK_SIZE");
        }
    }
    
    protected static String bytesToString(byte[] bytes)
    {
        String stringConverted = new String();
        for (int i = 0; i < bytes.length; i++)
            stringConverted = stringConverted + Integer.toHexString((bytes[i] & 0xFF) | 0x100).substring(1,3);
        return stringConverted;
    }
}