package org.epis.owasp;

import java.io.Serializable;
import javax.xml.bind.DatatypeConverter;

public class SecuredMessage implements Serializable
{
    private static final long serialVersionUID = 5278817120076948314L;
    
    private byte[] digitalSignature;
    private HeaderSecuredMessage header;
    private byte[] encryptedMessage;

    public SecuredMessage()
    {
        this.digitalSignature = null;
        this.header = null;
        this.encryptedMessage = null;
    }

    public SecuredMessage(byte[] digitalSignature, HeaderSecuredMessage header, byte[] encryptedMessage) {
        this.digitalSignature = digitalSignature;
        this.header = header;
        this.encryptedMessage = encryptedMessage;
    }
    
    public SecuredMessage(String base64DigitalSignature, String base64Header, String base64EncryptedMessage) throws OWASPException
    {
        this.digitalSignature = DatatypeConverter.parseBase64Binary(base64DigitalSignature);
        this.header = new HeaderSecuredMessage(base64Header);
        this.encryptedMessage = DatatypeConverter.parseBase64Binary(base64EncryptedMessage);
    }
    
    public SecuredMessage(String fullMessage) throws OWASPException
    {
        String[] parts = fullMessage.split("$");
        if(parts.length != 3)
            throw new OWASPException("Secured message can't be parsed", "INVALID_SECURED_MESSAGE_LENGTH");
        this.digitalSignature = DatatypeConverter.parseBase64Binary(parts[0]);
        this.header = new HeaderSecuredMessage(parts[1]);
        this.encryptedMessage = DatatypeConverter.parseBase64Binary(parts[2]);
    }

    public byte[] getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(byte[] digitalSignature) {
        this.digitalSignature = digitalSignature;
    }

    public HeaderSecuredMessage getHeader() {
        return header;
    }

    public void setHeader(HeaderSecuredMessage header) {
        this.header = header;
    }

    public byte[] getEncryptedMessage() {
        return encryptedMessage;
    }

    public void setEncryptedMessage(byte[] encryptedMessage) {
        this.encryptedMessage = encryptedMessage;
    }
        
    @Override
    public String toString() {
        return  DatatypeConverter.printBase64Binary(digitalSignature) + "$" +
                header.toString() + "$" +
                DatatypeConverter.printBase64Binary(encryptedMessage);
    }
}
