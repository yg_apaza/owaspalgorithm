package org.epis.owasp;

import java.io.Serializable;
import javax.xml.bind.DatatypeConverter;

public class HeaderSecuredMessage implements Serializable
{
    private static final long serialVersionUID = -8145884726586136382L;
    
    private byte[] encryptedSymmetricKey;
    private String hashMessage;

    public HeaderSecuredMessage()
    {
        this.encryptedSymmetricKey = null;
        this.hashMessage = null;
    }

    public HeaderSecuredMessage(byte[] encryptedSymmetricKey, String hashMessage)
    {
        this.encryptedSymmetricKey = encryptedSymmetricKey;
        this.hashMessage = hashMessage;
    }
    
    public HeaderSecuredMessage(String base64EncryptedSymmetricKey, String hashMessage)
    {
        this.encryptedSymmetricKey = DatatypeConverter.parseBase64Binary(base64EncryptedSymmetricKey);
        this.hashMessage = hashMessage;
    }
    
    public HeaderSecuredMessage(String fullMessage) throws OWASPException
    {
        String[] parts = fullMessage.split("|");
        if(parts.length != 2)
            throw new OWASPException("Header of secured message can't be parsed", "INVALID_HEADER_SECURED_MESSAGE_LENGTH");
        this.encryptedSymmetricKey = DatatypeConverter.parseBase64Binary(parts[0]);
        this.hashMessage = parts[1];
    }

    public byte[] getEncryptedSymmetricKey() {
        return encryptedSymmetricKey;
    }

    public void setEncryptedSymmetricKey(byte[] encryptedSymmetricKey) {
        this.encryptedSymmetricKey = encryptedSymmetricKey;
    }

    public String getHashMessage() {
        return hashMessage;
    }

    public void setHashMessage(String hashMessage) {
        this.hashMessage = hashMessage;
    }
    
    @Override
    public String toString() {
        return  DatatypeConverter.printBase64Binary(encryptedSymmetricKey) + "|" +
                hashMessage;
    }
}
