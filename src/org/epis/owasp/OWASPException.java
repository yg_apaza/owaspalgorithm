package org.epis.owasp;

import java.io.Serializable;

public class OWASPException extends Exception implements Serializable
{
    private static final long serialVersionUID = 2721806883414960223L;
    
    private String errorCode = "UNKNOWN_EXCEPTION";
    
    public OWASPException(String message, String errorCode)
    {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode()
    {
        return this.errorCode;
    }
}
